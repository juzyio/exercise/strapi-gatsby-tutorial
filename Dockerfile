FROM node:12.16.1-alpine

WORKDIR /srv/app

# Since we use app builder instead of package.json, after app created at container, lets copy back the package.json to host
RUN yarn create strapi-app backend --quickstart --no-run

WORKDIR /srv/app/backend
RUN yarn strapi install graphql

# EXPOSE 1337 No need expose here as docker-compose will take the role

CMD ["yarn", "develop"]