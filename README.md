Dockerized Strapi & Gatsby blog, with sqllite (build-in)
Since no persisted database, please create following content in Strapi thru admin panel
- article
    - title with type Text (required)
    - content with type Rich Text (required)
    - image with type Media (Single image) and (required)
    - published_at with type date (required)
- category
    - name with type Text (required)

More detail at: https://strapi.io/blog/build-a-static-blog-with-gatsby-and-strapi